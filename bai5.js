// bài 5: tính tổng 2 ký số
// input khai báo 2 biến ký số cho người dùng nhập
/** progess: B1 lấy giá trị của 2 biến trên cộng lại theo dạng string => ra được tổng ký số 
 * B2: phân tích ra đơn vị hàng chục và hàng đơn vị bằng phép chia làm tròn và phép chia lấy dư
 * B3: cộng 2 đơn vị hàng chục và hàng đươn vị => kết quả
 */
// ouput: show kết quả ký số

function reslutShow5(){
    var kySo1EL= document.getElementById("kySo1").value;
    var kySo2EL= document.getElementById("kySo2").value;

    var tongKySo= kySo1EL + kySo2EL;
    
    var hangChuc = Math.floor(tongKySo /10);
    var hangDV  =  tongKySo %10;
     
    var kySoKQ = hangChuc + hangDV ; 

    var tongKySoKQ = document.getElementById("result5");
    tongKySoKQ.innerHTML = `
    <h1> Kết Quả : ${kySoKQ} </h1>
    `
}