// bài 1 : Tính tiền lương nhân viên
// Mô hình 3 khối
// input : lấy dữ liệu người dùng nhập vào 2 ô lương 1 ngày và số ngày làm việc
/** progess:
 *  tạo biến 3  số ngày làm việc  , lương 1 ngày .
 *  gán giá trị cho biến kết quả thông qua dữ liệu của biên = số ngày làm việc * lương 1 ngày.
 * in ra kết quả ra thẻ h1 bằng biến resultEL có thuộc tính innerHTML
 */
// output: tổng lương

function resultShow1(){
    var tienLuong1Ngay = document.getElementById("luong1Ngay");
    var soNgayLam = document.getElementById("soNgayLam");
    var ketQua = tienLuong1Ngay.value * soNgayLam.value;
    var resultEL = document.getElementById("result");
    resultEL.innerHTML =  `
    <h1 > Tổng Lương : ${ketQua} </h1>
    `;
    
}